module.exports = {
  purge: [
    './dist/*.html'
  ],
  theme: {
    extend: {},
  },
  variants: {
    display: ["group-hover", 'responsive'],
    textColor: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
    backgroundColor: ['responsive', 'hover', 'focus', 'group-hover']
  },
  plugins: [],
}
